import os
from os.path import dirname, abspath, join
from dotenv import load_dotenv
import pymysql

APP_DIR = abspath(dirname(__file__))
dotenv_path = join(APP_DIR, '.env')
load_dotenv(dotenv_path)

class DB:
    
    def __init__(self):
        self.__DB_TABLE= os.environ.get('DB_TABLE')    
        try:
            self.connection = pymysql.connect(
                host=os.environ.get('DB_HOST'),
                port=int(os.environ.get('DB_PORT')),
                database=os.environ.get('DB_DATABASE'),
                user=os.environ.get('DB_USERNAME'),
                password=os.environ.get('DB_PASSWORD'),
            )
            self.cursor = self.connection.cursor()
            print("conexión establecida")
            self.__getServerInfo()
        except pymysql.Error:
            print("error!!")
    def __getServerInfo(self):
        info = self.connection.get_server_info() 
        print("servidor MySQL info: ",info)
    def getPeople(self):
        sql = 'select * FROM {}'.format(self.__DB_TABLE)
        self.cursor.execute(sql)
        result = self.cursor.fetchall()
        print("----")
        print(self.__DB_TABLE)
        return result
    def getPeopleByName(self, name):
        sql = "SELECT * FROM {} WHERE name = '{}'".format(self.__DB_TABLE,name)
        self.cursor.execute(sql)
        result = self.cursor.fetchone()
        print(result)
    def getPeopleById(self, id):
        print("getPeopleById...")
        table=str(self.__DB_TABLE)
        query='SELECT * FROM people WHERE id = %s'
        self.cursor.execute(query,(id,))
        result = self.cursor.fetchone()
        print(result)
        return result
    def addPeople(self,name, birth, geo):
        query="INSERT INTO people (name, birth, geo) VALUES (%s, %s, %s)"
        self.cursor.execute(query,(name, birth, geo))
        self.connection.commit()
        print("Persona {} nacida el {} con ubicación: {}; almacenada correctamente.".format(name, birth, geo))
    def deletePeople(self, id):
        query = "DELETE FROM people WHERE id = %s"
        self.cursor.execute(query,(id,))
        self.connection.commit()
        print("Id: {} Eliminado corectamente.".format(id))
    def updatePeople(self,id,name, birth, geo):
        print(id,name,birth,geo)
        query = "UPDATE people SET name=%s, birth=%s, geo=%s WHERE id = %s"
        self.cursor.execute(query, (name,birth,geo,id))
        self.connection.commit()
        print("Id: {} actualizado.".format(id))
    def createTable(self):
        sql = "SHOW TABLES LIKE 'people'"
        self.cursor.execute(sql)
        data = self.cursor.fetchone()
        #print("resultado de busqueda de tabla...",data)
        if data:
            print("La tabla ya ha sido creada...")
        else:
            print("La tabla no está creada...creando...")
            sql = "CREATE TABLE people (id INT AUTO_INCREMENT, name VARCHAR(20), birth DATE, geo VARCHAR(20), PRIMARY KEY (id));"
            self.cursor.execute(sql)
            self.connection.commit()
            print("Tabla Creada...")