# from flask_bootstrap import Bootstrap
import re
from flask import json, jsonify
from flask import Flask, render_template, redirect, request, flash
from pymysql import NULL
from db import DB
import folium
import forms
from datetime import date
from flask_wtf import CSRFProtect

navigation={
    "home": "/",
    "people": "/people",
    "about": "/about",
    "update": "/update/<int:id>",
    "delete": "/delete/<id>",
    "edit" : "/edit/<int:id>",
    "list": "/list"
}
        
db=DB()
db.createTable()

app = Flask(__name__)

app.secret_key="bolsaDeTelgopor"
Csrf= CSRFProtect(app)


@app.route(navigation["home"], methods=['GET', 'POST'])
def home():
    
    return redirect('/people')

@app.route(navigation["people"], methods=['GET','POST'])
def addPeople():
    print("Adding a new field...")
    fields= forms.Form_Data()
        
    if fields.validate_on_submit():
        data= request.form.to_dict()
        print (data)
        csrf_token, people, birth, geo = data.values()
        db.addPeople(people, birth, geo)
        flash('Nuevo Usuario Agregado Satisfactoriamente')
        return redirect('/people')
    elif fields.is_submitted():
        print("----Hubieron errores----")
        print(fields.errors)

    return render_template("home.html",form=fields, action='/people')

@app.route(navigation["delete"], methods=['GET'])
def delePeople(id):
    db.deletePeople(id)
    flash("Registro eliminado...")
    return redirect("/list")

@app.route(navigation["edit"],methods=['GET','POST'])
def edit(id=""):
    url='/edit/{}'.format(id)
    fields= forms.Form_Data()
    if fields.validate_on_submit():
        
            print("los datos son válidos")
            data= request.form.to_dict()
            csrf_token, name, birth, geo = data.values()
            print(csrf_token)
            db.updatePeople(id,name, birth, geo)
            flash('Edición de Usuario realizada Satisfactoriamente')
            return redirect("/list")
    elif fields.is_submitted():
        print("los datos NO SON válidos..")
        print(fields.errors)
        flash('Hubo un problema al editar el usuario')
    
    user = db.getPeopleById(id)
    if not user:
        print("user don't exist")
        return render_template('error.html')
    else:
        print("user exist...")
        user=db.getPeopleById(id)
        print(user)
        fields= forms.Form_Data(people=user[1], birth=user[2],geo=user[3])
    return render_template("home.html",form=fields, action=url)

@app.route(navigation["list"], methods=['GET'])
def people():
    resp =db.getPeople()
    return render_template('list.html', data=resp)
@app.route('/test')
def test():
    resp =db.getPeople()
    return render_template('test.html', resp=resp)

@app.route('/map')
def index():
    resp =db.getPeople()
    start_coords = (-31.5369,-68.5416)
    folium_map = folium.Map(location=start_coords, zoom_start=4)

    for item in resp:
        split= item[3].split(',')
        temp= item[2]
        date=temp.strftime("%m/%d/%Y")
        print(temp)        
        folium.Marker(
            location=[float(split[0]), float(split[1])],
            popup=folium.Popup("<h1>"+item[1]+"</h1>"+"<h3>Cumpleaños:"+date+"</h3>"),
        ).add_to(folium_map)
    return folium_map._repr_html_()

@app.errorhandler(404)
def error(e):
    return render_template('error.html')
if __name__ == '__main__':
    app.config['JSONIFY_PRETTYPRINT_REGULAR'] = True
    app.run(debug=True, port=3600)
