from wtforms import Form
from flask_wtf import FlaskForm
from wtforms import StringField, TextField, SelectField, RadioField, SelectMultipleField
from wtforms.fields.html5 import DateField
from wtforms import validators
from wtforms.validators import Optional, DataRequired
from datetime import date

def validateGeo(form, field):
    print(len(field.data))
    if len(field.data)> 17 and len(field.data) < 15:

        raise validators.ValidationError("La georeferencia no es correcta")

class Form_Data(FlaskForm):
    people = StringField('People',
        [
            validators.Length(min=5, max=20, message="Introduce un nombre válido: 5 a 20 caracteres")
        ]
        )
    birth = DateField('fecha de nacimiento', validators=[DataRequired(message="Fecha incorrecta")])
    # birth = DateField('fecha de nacimiento', validators=[DataRequired(message="Fecha incorrecta")], default= date.today)
    geo = TextField('georeferencia', [validateGeo])
    # rol = SelectField(u'Rol')
    # radios = RadioField('Seleccionar', choices=[('uno', '11'),('dos','22')])
    # opciones= SelectMultipleField('opciones multiples', choices=[('uno', '11'),('dos','22')])